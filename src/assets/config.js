/**
 * 所有的JavaScript环境配置信息都会被存放在这个文件之中
 */

/**
 *第一步配置api接口的请求地址，在开发环境和测试环境分别指向不同的路径
 */
var api_url = '';

switch (process.env.NODE_ENV) {
    case 'development':
        api_url = 'http://localhost:8080/api';
        break;
    case 'production':
        api_url = 'https://www.yiliyuan.club/api';
        break;
}

export const YILIYUAN_CONFIG = {
    API_URL:api_url,
}