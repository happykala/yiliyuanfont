import {YILIYUAN_CONFIG } from "../../config";
import axios from 'axios';
export default {
    /**
     * @abstract GET /products 请求所有的产品数据
     * @returns {*}
     */
    getProducts: function () {
        return axios.get(YILIYUAN_CONFIG.API_URL + '/products');
    },

    /**
     * @abstract GET /products/{productId} 根据产品id获取产品的具体数据
     * @param productId
     * @returns {*}
     */
    getProduct: function (productId) {
        return axios.get(YILIYUAN_CONFIG.API_URL + '/products/' + productId);
    },

    /**
     * @abstract POST /products 新增一个产品记录
     * @param title
     * @param price
     * @param simpledes
     * @param des
     * @param path
     * @param creator
     * @returns {*}
     */
    postAddNewProduct: function (title,price,simpledes,des,path,creator) {
        return axios.post(YILIYUAN_CONFIG.API_URL + 'products',{
            title: title,
            price: price,
            simpledes: simpledes,
            des: des,
            path: path,
            creator: creator
        })
    },

    test: function(){
        console.log(YILIYUAN_CONFIG.API_URL);
    }
}